const emptyChartDatasets = () => [
  {label: 'Imsak',   data: [], borderWidth: 1, hidden: true },
  {label: 'Fajr',    data: [], borderWidth: 1 },
  {label: 'Sunrise', data: [], borderWidth: 1, hidden: true },
  {label: 'Dhuhr',   data: [], borderWidth: 1 },
  {label: 'Asr',     data: [], borderWidth: 1 },
  {label: 'Maghrib', data: [], borderWidth: 1 },
  {label: 'Isha',    data: [], borderWidth: 1 },
];

const PrayerChart = {
  chart: null,

  init: function (ctx) {
    if (PrayerChart.chart !== null) {
      return;
    }

    const futureDate = `${moment().year() + 100}-01-01`;
    PrayerChart.chart = new Chart(ctx, {
      type: 'line',
      options: {
        scales: {
          x: {
            type: 'time',
            position: 'bottom',
            time: {
              unit: 'day',
              displayFormats: {
                day: 'ddd DD MMM'
              }
            }
          },
          y: {
            reverse: true,
            type: 'linear',
            position: 'left',
            ticks: {
              min: moment(`${futureDate} 00:00:00`).valueOf(),
              max: moment(`${futureDate} 23:59:59`).valueOf(),
              stepSize: 3.6e+6,
              beginAtZero: false,
              callback: value => {
                let date = moment(value);
                if (date.diff(moment(`${futureDate} 23:59:59`), 'minutes') === 0) {
                  return null;
                }

                return date.format('h A');
              }
            }
          }
        },
        plugins: {
          tooltip: {
            callbacks: {
              title: function (context) {
                return moment(context[0].parsed.x).format('ddd DD MMM');
              },
              label: function (context) {
                return `${context.dataset.label}: ${moment(context.parsed.y).format('h:mm A')}`;
              }
            }
          }
        }
      }
    })
  },

  updateChartDatasets: function(e) {
    PrayerChart.chart.data.datasets = e.detail;
    PrayerChart.chart.update();
  }
}
