const AlAdhan = {
  apiUrl: 'https://api.aladhan.com/v1',

  fetchMonthly: function(year, month, city = 'Melbourne', country = 'Australia', method = 3, school = 1) {
    const callUrl = `${AlAdhan.apiUrl}/calendarByCity?` +
      new URLSearchParams({
        city: city,
        country: country,
        method: method,
        month: month,
        year: year,
        school: school,
      }
    );

    return fetch(callUrl)
      .then(response => response.json())
      .then(data => {
        document.dispatchEvent(new CustomEvent('alAdhanMonthlyDataLoaded', {detail: data.data}));
      });
  },
}

