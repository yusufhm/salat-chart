const State = {
  chartCanvas: document.getElementById('salat-chart'),
  loading: true,

  populateChartDatasets: function(e) {
    const datasets = emptyChartDatasets();

    // Populate the datasets.
    e.detail.map(day => {
      const pDate = new Date(day.date.readable)
      const futureDate = `${pDate.getFullYear() + 100}-01-01`;
      for (let i = 0; i < datasets.length; i++) {
        prayerName = datasets[i].label;
        prayerTime = day.timings[prayerName].split(' ')[0];
        datasets[i].data.push({
          x: moment(day.date.gregorian.date, day.date.gregorian.format).format('YYYY-MM-DD'),
          y: moment(`${futureDate} ${prayerTime}`).valueOf()
        });
      }
    });

    State.chartCanvas.dispatchEvent(new CustomEvent('chartDatasetsPopulated', {detail: datasets}));
    State.loading = false;
  },

};

document.addEventListener('alAdhanMonthlyDataLoaded', State.populateChartDatasets);
State.chartCanvas.addEventListener('chartDatasetsPopulated', PrayerChart.updateChartDatasets);

const today = new Date();
AlAdhan.fetchMonthly(today.getFullYear(), today.getMonth() + 1);
PrayerChart.init(State.chartCanvas);
